package com.emilio.empresas

import android.app.Application
import android.content.Context
import com.emilio.empresas.di.*

class AppClass : Application() {
    lateinit var appComponent: AppComponent
    lateinit var context: Context

    override fun onCreate() {
        super.onCreate()
        context = this
        appComponent = DaggerAppComponent.builder()
            .contextModule(ContextModule(this))
            .okHttpClientModule(OkHttpClientModule())
            .build()
    }
}