package com.emilio.empresas.search

import android.os.Handler
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.emilio.empresas.login.Repository
import com.emilio.empresas.models.SearchResponse
import com.emilio.empresas.utils.ApiResponse
import com.emilio.empresas.utils.RequestStatus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchViewModel
@Inject constructor(
    private val repository: Repository
) : ViewModel() {
    companion object {
        private const val SEARCH_DELAY = 300L
    }

    private val disposables = CompositeDisposable()
    private val responseLiveData = MutableLiveData<ApiResponse<SearchResponse?>>()
    private val handler = Handler()
    private var lastQueryString: String? = null

    fun searchResponse(): MutableLiveData<ApiResponse<SearchResponse?>> {
        return responseLiveData
    }

    fun query(query: String) {
        if(query.isEmpty()) {
            return
        }

        lastQueryString = query
        handler.removeCallbacksAndMessages(null)

        handler.postDelayed({
            Log.d("searching...", query)

            disposables.add(repository.query(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { responseLiveData.setValue(ApiResponse(RequestStatus.LOADING, null, null)) }
                .subscribe({ result ->
                    if (lastQueryString == null || lastQueryString == query) {
                        val searchResponse = result.body()

                        Log.d(
                            "searching result for...",
                            "$query - ${searchResponse?.enterprises?.size} items"
                        )
                        searchResponse?.let {
                            responseLiveData.setValue(ApiResponse(RequestStatus.SUCCESS, it, null))
                        }
                    }
                },
                    { throwable -> responseLiveData.setValue(ApiResponse(RequestStatus.ERROR, null, throwable)) }
                ))
        }, SEARCH_DELAY)
    }

    override fun onCleared() {
        disposables.clear()
    }
}