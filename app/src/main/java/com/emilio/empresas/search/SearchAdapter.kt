package com.emilio.empresas.search

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.emilio.empresas.R
import com.emilio.empresas.models.Enterprise
import com.emilio.empresas.utils.layoutInflater

class SearchAdapter(
    context: Context,
    val onItemClick: (item: Enterprise) -> Unit
) : RecyclerView.Adapter<SearchViewHolder>() {
    private val layoutInflater = context.layoutInflater
    private var searchItems = listOf<Enterprise>()

    fun loadItems(newItems: List<Enterprise>) {
        searchItems = newItems
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val root = layoutInflater.inflate(R.layout.search_result_item, parent, false)
        return SearchViewHolder(root)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val item = searchItems[position]
        holder.enterprise = item
        holder.itemView.setOnClickListener { onItemClick(item) }
    }

    override fun getItemCount() = searchItems.size
}

class SearchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    init {
        ButterKnife.bind(this, itemView)
    }

    @BindView(R.id.image_view)
    lateinit var image: AppCompatImageView

    @BindView(R.id.title)
    lateinit var title: AppCompatTextView

    @BindView(R.id.business)
    lateinit var business: AppCompatTextView

    @BindView(R.id.country)
    lateinit var country: AppCompatTextView

    var enterprise: Enterprise? = null
        set(value) {
            field = value
            title.text = value?.name
            business.text = value?.type?.name
            country.text = value?.country

            Glide.with(itemView.context).load(value?.photo)
                .transition(DrawableTransitionOptions.withCrossFade())
                .apply(
                    RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.enterprise_placeholder)
                )
                .into(image)
        }
}