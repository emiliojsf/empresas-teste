package com.emilio.empresas.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.emilio.empresas.login.LoginViewModel
import com.emilio.empresas.login.Repository
import com.emilio.empresas.search.SearchViewModel
import io.reactivex.annotations.NonNull
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ViewModelFactory
@Inject constructor(
    private val repository: Repository,
    private val sessionProvider: SessionProvider
) : ViewModelProvider.Factory {
    @NonNull
    override fun <T : ViewModel> create(@NonNull modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(repository, sessionProvider) as T
        }
        if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
            return SearchViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}