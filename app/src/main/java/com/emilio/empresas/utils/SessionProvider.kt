package com.emilio.empresas.utils

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.emilio.empresas.di.OkHttpClientModule
import com.emilio.empresas.login.Session
import com.emilio.empresas.models.LoginResponse
import com.google.gson.Gson
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SessionProvider
@Inject
constructor(context: Context) {
    companion object {
        private const val SESSION_PREFS = "session_prefs"
    }

    private var session: Session? = null
    private var sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context)

    init {
        setOkHttpClientHeaders()
    }

    fun setSession(loginResponse: LoginResponse) {
        Log.d("search", "setting headers up ... token: ${loginResponse.loginHeaders?.accessToken}")

        session = Session().apply {
            this.accessToken = loginResponse.loginHeaders?.accessToken
            this.client = loginResponse.loginHeaders?.client
            this.uid = loginResponse.loginHeaders?.uid
            this.name = loginResponse.investor?.name
            this.photo = loginResponse.investor?.photo
        }

        setOkHttpClientHeaders()
        sharedPrefs.edit().putString(SESSION_PREFS, Gson().toJson(session)).apply()
    }

    fun getSession() : Session? = session ?: Gson().fromJson(getSessionFromSharedPrefs(), Session::class.java)

    private fun getSessionFromSharedPrefs() = sharedPrefs?.getString(SESSION_PREFS, null)

    private fun setOkHttpClientHeaders() {
        OkHttpClientModule.headers.apply {
            getSession()?.let {
                put("access-token", it.accessToken ?: "")
                put("client", it.client ?: "")
                put("uid", it.uid ?: "")
            }
        }
    }

    fun logout() {
        OkHttpClientModule.headers.clear()
        session = null

        sharedPrefs.edit().remove(SESSION_PREFS).apply()
    }
}