package com.emilio.empresas.utils

import android.content.Context
import android.net.ConnectivityManager

object Constant {
    fun checkInternetConnection(context: Context): Boolean {
        val connectivity = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager


        val activeNetwork = connectivity.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }

}
