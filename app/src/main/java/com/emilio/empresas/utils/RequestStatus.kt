package com.emilio.empresas.utils

enum class RequestStatus {
    LOADING,
    SUCCESS,
    ERROR
}