package com.emilio.empresas.utils

object Urls {
    const val BASE_URL = "http://empresas.ioasys.com.br/api/v1/"
    const val LOGIN = "users/auth/sign_in"
    const val QUERY = "enterprises"
}