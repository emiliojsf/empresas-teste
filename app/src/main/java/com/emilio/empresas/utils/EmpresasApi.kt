package com.emilio.empresas.utils

import com.emilio.empresas.models.LoginResponse
import com.emilio.empresas.models.SearchResponse
import com.emilio.empresas.models.User
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface EmpresasApi {
    @POST(Urls.LOGIN)
    fun login(@Body user: User): Observable<Response<LoginResponse>>

    @GET(Urls.QUERY)
    fun query(@Query("enterprise_types") type: Int = 1, @Query("name") query: String): Observable<Response<SearchResponse>>
}