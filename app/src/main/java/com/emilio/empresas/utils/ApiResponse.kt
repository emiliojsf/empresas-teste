package com.emilio.empresas.utils

import io.reactivex.annotations.Nullable

class ApiResponse<T> constructor(
    val status: RequestStatus,
    @param:Nullable @field:Nullable val data: T?,
    @param:Nullable @field:Nullable val error: Throwable?
)