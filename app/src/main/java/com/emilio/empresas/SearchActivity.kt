package com.emilio.empresas

import android.app.ActivityOptions
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Pair
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.emilio.empresas.models.Enterprise
import com.emilio.empresas.models.SearchResponse
import com.emilio.empresas.search.SearchAdapter
import com.emilio.empresas.search.SearchViewModel
import com.emilio.empresas.utils.ApiResponse
import com.emilio.empresas.utils.RequestStatus
import com.emilio.empresas.utils.SessionProvider
import com.emilio.empresas.utils.ViewModelFactory
import javax.inject.Inject

class SearchActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var sessionProvider: SessionProvider

    private var viewModel: SearchViewModel? = null

    @BindView(R.id.recycler_view)
    lateinit var recyclerView: RecyclerView

    private lateinit var searchAdapter: SearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        ButterKnife.bind(this)
        (application as AppClass).appComponent.doInjection(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)

        viewModel?.searchResponse()?.observe(this, Observer { response -> consumeResponse(response) })

        searchAdapter = SearchAdapter(this, this::onItemClick)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@SearchActivity)
            adapter = searchAdapter
        }
    }

    private fun consumeResponse(response: ApiResponse<SearchResponse?>) {
        when (response.status) {
            RequestStatus.LOADING -> {
            }

            RequestStatus.SUCCESS -> {
                response.data?.enterprises?.let {
                    searchAdapter.loadItems(it)
                }
            }

            RequestStatus.ERROR -> {
                Toast.makeText(this, resources.getString(R.string.error_string), Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun onItemClick(item: Enterprise) {
        val intent = Intent(this, EnterpriseActivity::class.java)
        intent.putExtra(EnterpriseActivity.EXTRA_TITLE, item.name)
        intent.putExtra(EnterpriseActivity.EXTRA_IMAGE_URL, item.photo)
        intent.putExtra(EnterpriseActivity.EXTRA_DESCRIPTION, item.description)

        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_activity_menu, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.search).actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    newText?.let {
                        viewModel?.query(it)
                    }
                    return true
                }

            })
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
            R.id.logout -> {
                sessionProvider.logout()
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}
