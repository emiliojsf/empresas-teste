package com.emilio.empresas.di

import com.emilio.empresas.LoginActivity
import com.emilio.empresas.SearchActivity
import dagger.Component
import javax.inject.Singleton

@Component(
    modules = [
        ContextModule::class,
        EmpresasModule::class,
        ViewModelFactoryModule::class
    ]
)
@Singleton
interface AppComponent {
    fun doInjection(loginActivity: LoginActivity)
    fun doInjection(searchActivity: SearchActivity)
}