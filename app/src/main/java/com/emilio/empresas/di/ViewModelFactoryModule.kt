package com.emilio.empresas.di

import androidx.lifecycle.ViewModelProvider
import com.emilio.empresas.utils.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module(includes = [ContextModule::class])
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}