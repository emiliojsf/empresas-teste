package com.emilio.empresas.di

import com.emilio.empresas.utils.EmpresasApi
import com.emilio.empresas.utils.Urls
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.Gson
import dagger.Provides
import com.google.gson.GsonBuilder
import dagger.Module
import retrofit2.Retrofit
import okhttp3.OkHttpClient
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import javax.inject.Singleton

@Module(includes = [OkHttpClientModule::class])
class EmpresasModule {

    @Provides
    @Singleton
    fun empresasApi(retrofit: Retrofit): EmpresasApi {
        return retrofit.create(EmpresasApi::class.java)
    }

    @Provides
    @Singleton
    fun retrofit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(Urls.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(gsonConverterFactory)
            .build()
    }

    @Provides
    fun gson(): Gson {
        val gsonBuilder = GsonBuilder()
        return gsonBuilder.create()
    }

    @Provides
    fun gsonConverterFactory(gson: Gson): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }
}