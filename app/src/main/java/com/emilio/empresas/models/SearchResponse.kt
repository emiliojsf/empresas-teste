package com.emilio.empresas.models

import com.emilio.empresas.utils.ProguardSafe
import com.google.gson.annotations.SerializedName

class SearchResponse : ProguardSafe {
    @SerializedName("enterprises")
    var enterprises: List<Enterprise>? = null
}