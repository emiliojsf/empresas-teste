package com.emilio.empresas.models

import com.emilio.empresas.utils.ProguardSafe
import com.google.gson.annotations.SerializedName

class Enterprise : ProguardSafe{
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("enterprise_name")
    var name: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("country")
    var country: String? = null

    @SerializedName("photo")
    var photo: String? = null

    @SerializedName("enterprise_type")
    var type: EnterpriseType? = null
}

class EnterpriseType : ProguardSafe {
    @SerializedName("enterprise_type_name")
    var name: String? = null
}