package com.emilio.empresas.models

data class LoginHeaders(
    var accessToken: String? = null,
    var client: String? = null,
    var uid: String? = null
)