package com.emilio.empresas.models

import com.emilio.empresas.utils.ProguardSafe
import com.google.gson.annotations.SerializedName

class LoginResponse : ProguardSafe {
    var loginHeaders: LoginHeaders? = null

    @SerializedName("investor")
    var investor: Investor? = null

    @SerializedName("enterprise")
    var enterprise: String? = null

    @SerializedName("success")
    var success: Boolean? = null
}