package com.emilio.empresas.models

import com.emilio.empresas.utils.ProguardSafe
import com.google.gson.annotations.SerializedName

class User (
    @SerializedName("email")
    var email: String? = null,
    @SerializedName("password")
    var password: String? = null) : ProguardSafe