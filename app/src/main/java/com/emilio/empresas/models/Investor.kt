package com.emilio.empresas.models

import com.emilio.empresas.utils.ProguardSafe
import com.google.gson.annotations.SerializedName

class Investor : ProguardSafe {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("investor_name")
    var name: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("country")
    var country: String? = null

    @SerializedName("balance")
    var balance: Long? = null

    @SerializedName("photo")
    var photo: String? = null

    @SerializedName("portfolio_value")
    var portfolioValue: Long? = null

    @SerializedName("first_access")
    var firstAccess: Boolean? = null

    @SerializedName("super_angel")
    var superAngel: Boolean? = null
}