package com.emilio.empresas.login

import com.emilio.empresas.models.LoginResponse
import com.emilio.empresas.models.SearchResponse
import com.emilio.empresas.models.User
import com.emilio.empresas.utils.EmpresasApi
import io.reactivex.Observable
import retrofit2.Response
import javax.inject.Inject

class Repository @Inject constructor(private val empresasApi: EmpresasApi) {

    fun executeLogin(email: String, password: String): Observable<Response<LoginResponse>> {
        return empresasApi.login(User(email, password))
    }

    fun query(query: String): Observable<Response<SearchResponse>> {
        return empresasApi.query(query = query)
    }
}