package com.emilio.empresas.login

import com.emilio.empresas.utils.ProguardSafe
import com.google.gson.annotations.SerializedName

data class Session(
    @SerializedName("access-token")
    var accessToken: String? = null,
    @SerializedName("client")
    var client: String? = null,
    @SerializedName("uid")
    var uid: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("photo")
    var photo: String? = null) : ProguardSafe