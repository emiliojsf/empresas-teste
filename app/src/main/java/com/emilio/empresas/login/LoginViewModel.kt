package com.emilio.empresas.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.emilio.empresas.models.LoginHeaders
import com.emilio.empresas.models.LoginResponse
import com.emilio.empresas.utils.ApiResponse
import com.emilio.empresas.utils.RequestStatus
import com.emilio.empresas.utils.SessionProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LoginViewModel
@Inject constructor(
    private val repository: Repository,
    private val sessionProvider: SessionProvider
) : ViewModel() {
    private val disposables = CompositeDisposable()
    private val responseLiveData = MutableLiveData<ApiResponse<LoginResponse?>>()

    fun loginResponse(): MutableLiveData<ApiResponse<LoginResponse?>> {
        return responseLiveData
    }

    fun hitLoginApi(email: String, password: String) {
        disposables.add(repository.executeLogin(email, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { responseLiveData.setValue(ApiResponse(RequestStatus.LOADING, null, null)) }
            .subscribe({ result ->
                val headers = result.headers()
                val loginHeaders = LoginHeaders().apply {
                    accessToken = headers["access-token"]
                    client = headers["client"]
                    uid = headers["uid"]
                }

                val loginResponse = result.body()?.apply {
                    this.loginHeaders = loginHeaders
                }

                loginResponse?.let {
                    sessionProvider.setSession(it)
                    responseLiveData.setValue(ApiResponse(RequestStatus.SUCCESS, it, null))
                }
            },
                { throwable -> responseLiveData.setValue(ApiResponse(RequestStatus.ERROR, null, throwable)) }
            ))
    }

    override fun onCleared() {
        disposables.clear()
    }
}