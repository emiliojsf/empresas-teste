package com.emilio.empresas

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ContentLoadingProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.emilio.empresas.login.LoginViewModel
import com.emilio.empresas.models.LoginResponse
import com.emilio.empresas.utils.*
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import javax.inject.Inject

class LoginActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var sessionProvider: SessionProvider

    @BindView(R.id.login_button)
    lateinit var loginButton: MaterialButton

    @BindView(R.id.progress_bar)
    lateinit var progressBar: ContentLoadingProgressBar

    @BindView(R.id.email_field)
    lateinit var email: TextInputLayout

    @BindView(R.id.password_field)
    lateinit var password: TextInputLayout

    var viewModel: LoginViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        ButterKnife.bind(this)
        (application as AppClass).appComponent.doInjection(this)

        sessionProvider.getSession()?.let {
            goToSearchActivity()
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
        viewModel?.loginResponse()?.observe(this, Observer { response -> consumeResponse(response) })

//        email.editText?.text?.replace(0, 0, "testeapple@ioasys.com.br")
//        password.editText?.text?.replace(0, 0, "12341234")
    }

    @OnClick(R.id.login_button)
    fun onLoginClicked() {
        if (isValid()) {
            if (!Constant.checkInternetConnection(this)) {
                Toast.makeText(this, resources.getString(R.string.network_error), Toast.LENGTH_SHORT)
                    .show()
            } else {
                viewModel?.hitLoginApi(email.editText?.text.toString(), password.editText?.text.toString())
            }

        }
    }

    private fun isValid(): Boolean {
        if (email.editText?.text.toString().trim().isEmpty()) {
            Toast.makeText(this, resources.getString(R.string.enter_valid_email), Toast.LENGTH_SHORT)
                .show()
            return false
        } else if (password.editText?.text.toString().trim().isEmpty()) {
            Toast.makeText(this, resources.getString(R.string.enter_valid_password), Toast.LENGTH_SHORT)
                .show()
            return false
        }

        return true
    }

    private fun consumeResponse(apiResponse: ApiResponse<LoginResponse?>) {
        when (apiResponse.status) {
            RequestStatus.LOADING -> {
                loginButton.animate().alpha(0.0F).start()
                progressBar.show()
            }

            RequestStatus.SUCCESS -> {
                apiResponse.data?.let {
                    goToSearchActivity()
                }
            }

            RequestStatus.ERROR -> {
                loginButton.animate().alpha(1.0F).start()
                progressBar.hide()
                Toast.makeText(this, resources.getString(R.string.error_string), Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun goToSearchActivity() {
        startActivity(Intent(this, SearchActivity::class.java))
        finish()
    }
}
