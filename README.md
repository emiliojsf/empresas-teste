## Como executar
1. Clone o projeto `git clone https://bitbucket.org/emiliojsf/empresas-teste`
2. `cd <empresas-teste>`
3. Execute a aplicação `./gradlew assembleDebug` ou, pelo Android Studio, clique no botão de Play

## Pendências

Não consegui escrever os testes unitários dentro do prazo estipulado.
Porém, foquei em utilizar a arquitetura que acredito ser, pelo que conversamos, o mais importante a se demonstrar nessa fase da avaliação.
Para isso, foi necessário um tempo de leitura e aprendizado para implementá-la, mas não muito.

Uma observação: enquanto eu fazia os testes, a API só retornava 2 valores no máximo. Além disso, muitos campos estavam nulos, principalmente o campo que deveria conter a url da imagem da empresa. Fiz os testes do meu lado e não identifiquei nenhum problema, ou seja, me parece problema da API mesmo. Talvez fosse interessante dar uma checada se está tudo certo na API.

## O que eu faria se tivesse mais tempo

- Sem dúvida, os testes unitários
- Melhor tratamento de erros e estados. Por exemplo, desenharia uma view para ser exibida quando desse erro na requisição por falta de internet, mostraria melhor para o usuário quando a pesquisa retornasse 0 elementos, mostraria views de loading sempre que necessário.
- Definiria uma animação melhor entre telas, utilizando transições até mesmo simples, como Shared Element Transitions.

## Bibliotecas Usadas

 - Retrofit, Glide, Gson (recomendadas)
 - RxAndroid - 
 - Dagger 2 - Utilizada para injeção de dependências
 - ButterKnife - Para binding de views
 - Lifecycle - Para uso de LiveData
 - Timber - Utilitário para Log

## Conclusão

Achei formidável finalmente pegar algo pra fazer utilizando MVVM. Aprendi muito nesses últimos dias devido a esse desafio.
Quero, acima de tudo, agradecer pela oportunidade.